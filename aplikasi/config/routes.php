<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
$route = [
	'default_controller' => 'Frontend',
	'newpassword/(:any)' => 'Frontend/newpassword/$1',
	'404_override' => '',
	'dashboard' => 'Backend',
	'jabatan' => 'Backend/jabatan',
	'pengguna' => 'Backend/pengguna',
	'ruang' => 'Backend/ruang',
	'barang' => 'Backend/barang',
	'peminjaman' => 'Backend/peminjaman',
	'riwayatlogin' => 'Backend/riwayatlogin',
	'logaktivitas' => 'Backend/logaktivitas',
	'logout' => 'Backend/logout',
	'backupdatabase' => 'Backend/backupdatabase',
];