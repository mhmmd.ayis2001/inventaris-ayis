<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// 089639462276
$autoload = [
	'packages' => [],
	'libraries' => ['session','database', 'pagination'],
	'helper' => ['url', 'security', 'ayis'],
	'drivers' => [],
	'config' => ['master'],
	'language' => [],
	'model' => []
];
