<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'core/Admin.php';

class Backend extends Admin {
	
	function __construct(){
		parent::__construct(); 
		setheader();
		clear_expired_session();
		$this->checklogin();
	}
	private function checklogin(){
		if($this->session->userdata('pengguna_id')){
     		return true;
		}else{
		    redirect(site_url());
		}
	}
	function getLatestUpdateAktivitas(){
		$get = query("SELECT id, nama as siapa, kategori, log as aktivitas FROM log ORDER BY tanggal DESC LIMIT 3");
		json($get->result());
	}
	function cekDurasiPeminjaman(){
		$pengguna_id = $this->session->userdata('pengguna_id');
		$check = query("SELECT id, durasi, tanggal FROM peminjam WHERE is_deleted IS NULL AND id_pengguna='$pengguna_id' AND status IS NULL AND return_on IS NULL");
		if ($check->num_rows() > 0) {
			$data = $check->row();
			$durasi = $data->durasi;
			$jatuhTempo = date('d-m-Y', strtotime($data->tanggal."+".$durasi.' days'));
			$sekarang = date('d-m-Y');
			if ($sekarang > $jatuhTempo) {
				$getDataPeminjam = query("SELECT id, nama, email, is_notify FROM pengguna WHERE id='$pengguna_id' AND is_deleted IS NULL");
				$getDataBarangYangDipinjam = query("SELECT p.id_pengguna, p.id_barang, b.nama as barang FROM peminjam p INNER JOIN barang b ON p.id_barang = b.id WHERE p.is_deleted IS NULL");
				$dataPeminjam = $getDataPeminjam->row();
				$dataBarang = $getDataBarangYangDipinjam->row();
				if ($dataPeminjam->is_notify === NULL) {
					$title = 'Pemberitahuan Aplikasi Inventaris';
					$email = $dataPeminjam->email;
					$subject = 'Pemberitahuan Pengembalian '.$dataBarang->barang;
					$header = 'Hai '.$dataPeminjam->nama.', <br/><br/>';
					$footer = '<br/><br/><br/><i>* Pesan ini dikirim otomatis oleh system.</i>';
					$message = $header.'Sudah waktunya kamu mengembalikan'.$dataBarang->barang.' kepetugas Inventaris'.$footer;
					$this->sendmail($title, $subject, $email, $message);
				}else{
					echo 'notified';
				}
			}else{
				echo 'pingpong';
			}
		}else{
			$response = $this->responses(false, 500, 'Tidak ada peminjaman atau semua sudah dikembalikan');
			json($response);
		}

	}
	public function logout(){
        $this->save_activity('keluar dari aplikasi', '', 'logout');
		$user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
    	$this->session->sess_destroy();
    	redirect(site_url(),'refresh');
	}
	public function masukan(){
		$data = [
			'judul' => post('judul'),
			'keterangan' => post('keterangan')
		];
		$simpan = $this->db->insert('masukan', $data);
		$lastid = $this->db->insert_id();
		if ($simpan) {
			$getMasukan = where('id', $lastid);
			$getMasukan = minta('masukan');
			$dataMasukan = $getMasukan->row();
			// send email notification when data masukan inserted.
			$title = 'Pemberitahuan Masukan Aplikasi File Manager';
			$email = 'hfzrmd@gmail.com';
			$subject = $dataMasukan->judul;
			$header = 'Hai Mas Hafiz Ramadhan, <br/><br/>';
			$footer = '<br/><br/><br/><i>* Pesan ini dikirim otomatis oleh system, tetap semangat ya!</i>';
			$message = $header.$dataMasukan->keterangan.$footer;
			$this->sendmail($title, $subject, $email, $message);
		}
	}
	function index(){
		$data = [ 'file' => 'modul/dashboard/index' ];
		interpreter($data);
	}
	function pengguna(){
		$data = [ 'file' => 'modul/pengguna/index' ];
		interpreter($data);
	}
	function getPengguna(){
		model('getPengguna');
	}
	function getJabatanData(){
		isajax();
		$data = where('is_deleted', null);
		$data = minta('jabatan');
		JSON($data->result());
	}
	function getPenggunaById(){
		$this->get_by_id(post('id'), 'pengguna');
	}
	function simpanPengguna(){
		isajax();
		$password = post('password');
		$hashPassword = password_hash($password, PASSWORD_DEFAULT);
		$data = [
			'nama' => post('nama'),
			'email' => post("email"),
			'id_jabatan' => post("jabatan"),
			'username' => post("username"),
			'katasandi' => $hashPassword
		];
		$simpan = $this->db->insert('Pengguna', $data);
		$lastid = $this->db->insert_id();
		if ($simpan) {
			$unggah = $this->unggah($lastid, 'static/file/pengguna', 'pengguna');
		}
	}
	function hapusPengguna(){
		$id = post('id');
		$this->soft_delete($id, 'pengguna');
	}
	function updatePengguna(){
		$data = [
			'nama' => post('nama'),
			'telepon' => post('telepon'),
			'username' => post('username'),
			'email' => post('email')
		];
		$jabatan = post('jabatan');
		$password = post('password');
		if ($password != '') {
			$password = password_hash($password, PASSWORD_DEFAULT);
			$data['katasandi'] = $password;
		}
		$update = $this->update(post('id'), $data, 'Pengguna');
		if ($update && (!empty($_FILES['file']['name']))) {
			$unggah = $this->unggah(post('id'), 'static/assets/images/profile', 'Pengguna');
		}
	}
	function ruang(){
		$data = [ 'file' => 'modul/ruang/index' ];
		interpreter($data);
	}
	function getRuang(){
		model('getRuang');
	}
	function getKategoriData(){
		$data = where('is_deleted', null);
		$data = minta('kategori');
		JSON($data->result());
	}
	function getRuangById(){
		$this->get_by_id(post('id'), 'ruang');
	}
	function simpanRuang(){
		isajax();
		$data = [
			'ruang' => post('ruang'),
			'id_kategori' => post('kategori')
		];
		$this->simpan($data, 'ruang');
	}
	function hapusRuang(){
		$id = post('id');
		$this->soft_delete($id, 'ruang');
	}
	function updateRuang(){
		$data = [
			'ruang' => post('ruang'),
			'id_kategori' => post('kategori')
		];
		$update = $this->update(post('id'), $data, 'ruang');
	}
	function barang(){
		$data = [ 'file' => 'modul/barang/index' ];
		interpreter($data);
	}
	function getKategori(){
		model('getKategori');
	}
	function simpanKategori(){
		isajax();
		$data = ['kategori' => post('kategori')];
		$this->simpan($data, 'kategori');
	}
	function getKategoriById(){
		$this->get_by_id(post('id'), 'kategori');
	}
	function updateKategori(){
		$data = ['kategori' => post('kategori')];	
		$update = $this->update(post('id'), $data, 'kategori');
	}
	function hapusKategori(){
		$this->soft_delete(post('id'), 'kategori');
	}
	function getBarang(){
		model('getBarang');
	}
	function getRuangByIdKategori(){
		$id = post('id');
		$sql = query("SELECT r.*, k.kategori FROM ruang r INNER JOIN kategori k ON r.id_kategori = k.id WHERE r.is_deleted IS NULL");
		json($sql->result());
	}
	function getSumberData(){
		$data = where('is_deleted', null);
		$data = minta('sumber');
		JSON($data->result());
	}
	function getBarangById(){
		$this->get_by_id(post('id'), 'barang');
	}
	function simpanBarang(){
		isajax();
		$data = [
			'id_ruang' => post('ruang'),
			'id_kategori' => post('kategori'),
			'sumber' => post('sumber'),
			'nama' => post('nama'),
			'jumlah' => post('jumlah'),
			'status' => 1
		];
		$this->simpan($data, 'barang');
	}
	function hapusBarang(){
		$id = post('id');
		$this->soft_delete($id, 'barang');
	}
	function updateBarang(){
		$data = [
			'id_ruang' => post('ruang'),
			'id_kategori' => post('kategori'),
			'sumber' => post('sumber'),
			'nama' => post('nama'),
			'jumlah' => post('jumlah'),
			'status' => 1
		];
		$update = $this->update(post('id'), $data, 'barang');
	}
	function peminjaman(){
		$data = ['file' => 'modul/peminjaman/index'];
		interpreter($data);	
	}
	function getPeminjaman(){
		model("getPeminjaman");
	}
	function getPenggunaData(){
		isajax();
		$minta = query("SELECT id, nama FROM pengguna WHERE is_deleted IS NULL");
		JSON($minta->result());
	}
	function getBarangData(){
		isajax();
		$minta = query("SELECT id, nama as barang FROM barang WHERE is_deleted IS NULL AND jumlah != 0");
		JSON($minta->result());	
	}
	function simpanPeminjaman(){
		isajax();
		$barang = post('barang');
		// check jumlah barang sebelum dipinjam
		$check = query("SELECT jumlah FROM barang WHERE is_deleted IS NULL AND id='$barang'");
		if ($check->num_rows() > 0) {
			$jumlah = post('jumlah');
			$pengguna = post('pengguna_');
			if ($pengguna === "") {
				$pengguna = $this->session->userdata('pengguna_id');
			}
			$data = [
				'id_pengguna' => $pengguna,
				'id_barang' => $barang,
				'jumlah' => $jumlah,
				'tanggal' => date('d-m-Y'),
				'durasi' => post('durasi')
			];
			$simpan = $this->db->insert('peminjam', $data);
			// $lastid = $this->db->insert_id();
			if ($simpan) {
				# update jumlah data yang terpinjam
				// getJumlah terakhir terlebih dahulu
				$getJumlah = query("SELECT jumlah FROM barang WHERE id='$barang'")->row();
				$data = $getJumlah->jumlah;
				$update = $data - $jumlah;
				$insertIntoDatabase = ['jumlah' => $update];
				$updateJumlahBarang = where('id', $barang);
				$updateJumlahBarang = update('barang', $insertIntoDatabase);
				if ($updateJumlahBarang) {
					$response = $this->responses(true, 200, 'Barang berhasil dipinjam');
					json($response);
				}else{
					$response = $this->responses(false, 500, 'Barang gagal dipinjam');
					json($response);

				}
			}else{
				$response = $this->responses(false, 500, 'Data peminjaman gagal di simpan');
				json($response);
			}
		}else{
			$response = $this->responses(false, 500, 'Stok Barang sudah habis');
			json($response);
		}
	}
	function konfirmasiPeminjaman(){
		$id = post('id');
		// ambil informasi peminjaman barang
		$peminjaman = query("SELECT p.*, b.id as idBarang, b.jumlah as jumlahBarang, b.nama as barang FROM peminjam p INNER JOIN barang b ON p.id_barang = b.id WHERE p.is_deleted IS NULL");
		$data =  $peminjaman->row();
		$jumlahBarang = $data->jumlahBarang;
		$jumlahPeminjaman = $data->jumlah;
		// jumlahBarang + jumlahPeminjaman;
		$tambahkembali = $jumlahBarang + $jumlahPeminjaman;
		$insertIntoDatabase = ['jumlah' => $tambahkembali];
		$updateJumlahBarang = where('id', $data->idBarang);
		$updateJumlahBarang = update('barang', $insertIntoDatabase);
		if ($updateJumlahBarang) {
			$dataForUpdatePeminjaman = [
				'status' => 1,
				'return_on' => date('d-m-Y H:i:s')
			];
			$this->update($id, $dataForUpdatePeminjaman, 'peminjam');
		}else{
			$response = $this->responses(false, 500, 'Data peminjaman gagal dikonfirmasi');
			json($response);
		}
	}
	function jabatan(){
		$data = ['file' => 'modul/jabatan/index'];
		interpreter($data);	
	}
	function getJabatan(){
		model("getJabatan");
	}
	function getJabatanById(){
		$this->get_by_id(post('id'), 'jabatan');
	}
	function updateJabatan(){
		$data = [
			'jabatan' => post('jabatan')
		];
		$this->update(post('id'), $data, 'jabatan');
	}
	function simpanJabatan(){
		$data = [
			'jabatan' => post('jabatan'),
		];
		$this->simpan($data, 'jabatan');
	}
	function hapusJabatan(){
		$id = post('id');
		$this->soft_delete($id, 'jabatan');
	}
}