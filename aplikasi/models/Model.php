<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends CI_Model {
	public function __construct(){ 
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		notget();
	}
	function getJabatan(){
		isajax();
		$minta = where('is_deleted', null);
		$minta = minta('jabatan');
		$hasil = $minta->result();
		$no = 0;
		$result = ['data' => []];
		$data = $hasil;
		foreach ($data as $key => $value) { $no++;
			$id = $value->id;
			
			$opsi = "<div id='opsi' style='cursor: pointer;'>
						<center>
							<a href='javascript:void(0)' class='edit' data-id='$id' id='edit-jabatan' data-toggle='tooltip' title='Klik untuk mengedit data'><i class='far fa-edit' aria-hidden='true'></i></a>
							<a href='javascript:void(0)' class='hapus' data-id='$id' id='hapus-jabatan' data-toggle='tooltip' title='Klik untuk menghapus data'><i class='fas fa-trash' aria-hidden='true'></i></a>
						</center>
	 				</div>";
	 				$result['data'][$key] = array(
						$no,
						$value->jabatan,
						$opsi
				);
		}
		JSON($result);
	}
	function getPengguna(){
		isajax();
		$minta = query("SELECT k.*, j.jabatan FROM pengguna k INNER JOIN jabatan j ON k.id_jabatan = j.id WHERE k.is_deleted IS NULL");
		$hasil = $minta->result();
		$no = 0;
		$result = ['data' => []];
		$data = $hasil;
		foreach ($data as $key => $value) { $no++;
			$id = $value->id;
			
			$opsi = "<div id='opsi' style='cursor: pointer;'>
						<center>
							<a href='javascript:void(0)' class='edit' data-id='$id' id='edit-karyawan' data-toggle='tooltip' title='Klik untuk mengedit data'><i class='far fa-edit' aria-hidden='true' onclick='ClearFormData('#formKaryawan')'></i></a>
							<a href='javascript:void(0)' class='hapus' data-id='$id' id='hapus-karyawan' data-toggle='tooltip' title='Klik untuk menghapus data'><i class='fas fa-trash' aria-hidden='true'></i></a>
						</center>
	 				</div>";
	 				$result['data'][$key] = array(
						$no,
						$value->nama,
						$value->jabatan,
						$value->email,
						$opsi
				);
		}
		JSON($result);
	}
	function getRuang(){
		isajax();
		$minta = query("SELECT r.*, k.kategori FROM ruang r INNER JOIN kategori k ON r.id_kategori = k.id WHERE r.is_deleted IS NULL");
		$hasil = $minta->result();
		$no = 0;
		$result = ['data' => []];
		$data = $hasil;
		foreach ($data as $key => $value) { $no++;
			$id = $value->id;
			
			$opsi = "<div id='opsi' style='cursor: pointer;'>
						<center>
							<a href='javascript:void(0)' class='edit' data-id='$id' id='edit-ruang' data-toggle='tooltip' title='Klik untuk mengedit data'><i class='far fa-edit' aria-hidden='true' onclick='ClearFormData('#formRuang')'></i></a>
							<a href='javascript:void(0)' class='hapus' data-id='$id' id='hapus-ruang' data-toggle='tooltip' title='Klik untuk menghapus data'><i class='fas fa-trash' aria-hidden='true'></i></a>
						</center>
	 				</div>";
	 				$result['data'][$key] = array(
						$no,
						$value->ruang,
						$value->kategori,
						$opsi
				);
		}
		JSON($result);
	}
	function getKategori(){
		isajax();
		$minta = query("SELECT * FROM kategori WHERE is_deleted IS NULL");
		$hasil = $minta->result();
		$no = 0;
		$result = ['data' => []];
		$data = $hasil;
		foreach ($data as $key => $value) { $no++;
			$id = $value->id;
			
			$opsi = "<div id='opsi' style='cursor: pointer;'>
						<center>
							<a href='javascript:void(0)' class='edit' data-id='$id' id='edit-kategori' data-toggle='tooltip' title='Klik untuk mengedit data'><i class='far fa-edit' aria-hidden='true' onclick='ClearFormData('#formRuang')'></i></a>
							<a href='javascript:void(0)' class='hapus' data-id='$id' id='hapus-kategori' data-toggle='tooltip' title='Klik untuk menghapus data'><i class='fas fa-trash' aria-hidden='true'></i></a>
						</center>
	 				</div>";
	 				$result['data'][$key] = array(
						$no,
						$value->kategori,
						$opsi
				);
		}
		JSON($result);
	}
	function getBarang(){
		isajax();
		$minta = query("SELECT b.*, r.ruang, k.kategori FROM barang b INNER JOIN ruang r ON b.id_ruang = r.id INNER JOIN kategori k ON b.id_kategori = k.id WHERE b.is_deleted IS NULL AND b.jumlah != 0");
		$hasil = $minta->result();
		$no = 0;
		$result = ['data' => []];
		$data = $hasil;
		foreach ($data as $key => $value) { $no++;
			$id = $value->id;
			$opsi = "<div id='opsi' style='cursor: pointer;'>
						<center>
							<a href='javascript:void(0)' class='edit' data-id='$id' id='edit-barang' data-toggle='tooltip' title='Klik untuk mengedit data'><i class='far fa-edit' aria-hidden='true' onclick='ClearFormData('#formRuang')'></i></a>
							<a href='javascript:void(0)' class='hapus' data-id='$id' id='hapus-barang' data-toggle='tooltip' title='Klik untuk menghapus data'><i class='fas fa-trash' aria-hidden='true'></i></a>
						</center>
	 				</div>";
	 				$result['data'][$key] = array(
						$no,
						$value->nama,
						$value->ruang,
						$value->kategori,
						$value->jumlah,
						$value->sumber,
						$opsi
				);
		}
		JSON($result);
	}
	function getPeminjaman(){
		isajax();
		#filter by sesion jabatan
		$session = $this->session->userdata('jabatan');
		if ($session === "Siswa") {
			$pengguna_id = $this->session->userdata('pengguna_id');
			$minta = query("SELECT p.*, u.nama, b.nama as barang FROM peminjam p INNER JOIN pengguna u ON p.id_pengguna = u.id INNER JOIN barang b ON p.id_barang = b.id WHERE p.is_deleted IS NULL AND p.id_pengguna='$pengguna_id'");
		}else{
			$minta = query("SELECT p.*, u.nama, b.nama as barang FROM peminjam p INNER JOIN pengguna u ON p.id_pengguna = u.id INNER JOIN barang b ON p.id_barang = b.id WHERE p.is_deleted IS NULL");
		}
		$hasil = $minta->result();
		$no = 0;
		$result = ['data' => []];
		$data = $hasil;
		foreach ($data as $key => $value) { $no++;
			$id = $value->id;
			$kembali = $value->return_on;
			$status = $value->status;
			if ($kembali === NULL) {
				$kembali = '-';
			}
			if ($status === NULL) {
				$status = '<span class="pcoded-badge label label-default">Belum dikembalikan</span>';
			}else{
				$status = '<span class="pcoded-badge label label-info">Sudah dikembalikan</span>';
			}
			if ($session === "Siswa") {
				$opsi = "<div id='opsi' style='cursor: pointer;'>
						<center>
							<a href='javascript:void(0)' class='lapor' data-id='$id' id='lapor-peminjaman' data-toggle='tooltip' title='Klik untuk melaporkan peminjaman'><i class='fas fa-list' aria-hidden='true' onclick='ClearFormData('#formPeminjaman')'></i></a>
							<a href='javascript:void(0)' class='konfirmasi' data-id='$id' id='konfirmasi-peminjaman' data-toggle='tooltip' title='Klik untuk mengembalikan peminjaman'><i class='fas fa-archive' aria-hidden='true' onclick='ClearFormData('#formPeminjaman')'></i></a>
							<a href='javascript:void(0)' class='edit' data-id='$id' id='edit-peminjaman' data-toggle='tooltip' title='Klik untuk mengedit data'><i class='far fa-edit' aria-hidden='true' onclick='ClearFormData('#formPeminjaman')'></i></a>
							<a href='javascript:void(0)' class='hapus' data-id='$id' id='hapus-peminjaman' data-toggle='tooltip' title='Klik untuk menghapus data'><i class='fas fa-trash' aria-hidden='true'></i></a>
						</center>
	 				</div>";
			}else{
					$opsi = "<div id='opsi' style='cursor: pointer;'>
							<center>
								<a href='javascript:void(0)' class='edit' data-id='$id' id='edit-peminjaman' data-toggle='tooltip' title='Klik untuk mengedit data'><i class='far fa-edit' aria-hidden='true' onclick='ClearFormData('#formPeminjaman')'></i></a>
								<a href='javascript:void(0)' class='hapus' data-id='$id' id='hapus-peminjaman' data-toggle='tooltip' title='Klik untuk menghapus data'><i class='fas fa-trash' aria-hidden='true'></i></a>
							</center>
		 				</div>";	
			}
	 				$result['data'][$key] = array(
						$no,
						$value->nama,
						$value->barang,
						$value->jumlah.' buah',
						$value->tanggal,
						$value->durasi.' hari',
						$status,
						$kembali,
						$opsi
				);
		}
		JSON($result);
	}
	function getRiwayatLogin(){
		isajax();
		$minta = query("SELECT r.*, k.nama FROM riwayatlogin r INNER JOIN karyawan k ON r.id_karyawan = k.id ORDER BY last_login DESC");
		$hasil = $minta->result();
		$no = 0;
		$result = ['data' => []];
		$data = $hasil;
		foreach ($data as $key => $value) { $no++;
			$id = $value->id;
	 				$result['data'][$key] = array(
						$no,
						$value->nama,
						$value->os,
						$value->browser,
						$value->ip_address,
						$value->version,
						$value->last_login
				);
		}
		JSON($result);
	}
	function getLogAktivitas(){
		isajax();
		$minta = query("SELECT * FROM log ORDER BY tanggal DESC");
		$hasil = $minta->result();
		$no = 0;
		$result = ['data' => []];
		$data = $hasil;
		foreach ($data as $key => $value) { $no++;
			$id = $value->id;
	 				$result['data'][$key] = array(
						$no,
						$value->nama,
						$value->jabatan,
						$value->tanggal,
						$value->log
				);
		}
		JSON($result);
	}

}