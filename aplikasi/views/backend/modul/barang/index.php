<div class="main-body">
    <div class="page-wrapper">
        <div class="page-body">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <ul class="nav nav-tabs  tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#barang" role="tab">Barang</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kategori" role="tab">Kategori</a>
                        </li>
                    </ul>
                        <div class="tab-content tabs card-block">

                            <div class="tab-pane active" id="barang" role="tabpanel">
                                <div class="card">
                                        <div class="card-header">
                                            <h5>Data Barang 
                                            <button href="#modalBarang" data-toggle="modal" class="btn waves-effect waves-light btn-grd-primary" onclick="ClearFormData('#formBarang')">Tambah Data</button></h5>
                                        </div>
                                        <div class="card-block">
                                            <table id="tableBarang" class="table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Nama Barang</th>
                                                        <th>Ruang</th>
                                                        <th>Kategori</th>
                                                        <th>Jumlah</th>
                                                        <th>Sumber</th>
                                                        <th>Opsi</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Nama Barang</th>
                                                        <th>Ruang</th>
                                                        <th>Kategori</th>
                                                        <th>Jumlah</th>
                                                        <th>Sumber</th>
                                                        <th>Opsi</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="modal fade  modal-flex" id="modalBarang" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Tambah Barang</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                    <div class="modal-body model-container">
                                                        <form id="formBarang" method="post">
                                                            <input type="hidden" name="id" id="id">
                                                            <div class="form-group row"> 
                                                                <label class="col-sm-2 col-form-label"> Nama* </label>
                                                                <div class="col-sm-10"> 
                                                                    <input type="text" name="nama" id="nama" class="form-control form-control-round" autofocus="on" autocomplete="off" autosave="off" required="on" placeholder="Masukan data Barang">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row"> 
                                                                <label class="col-sm-2 col-form-label"> Kategori*</label>
                                                                <div class="col-sm-10"> 
                                                                    <select id="data-kategori" class="form-control" name="kategori" required="on" disabled="true">
                                                                        <option id="defaultvalueDataKategori" disabled="on" selected="on"></option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row"> 
                                                                <label class="col-sm-2 col-form-label"> Ruang*</label>
                                                                <div class="col-sm-10"> 
                                                                    <select id="data-ruang" class="form-control" name="ruang" required="on" disabled="true">
                                                                        <option id="defaultvalueDataRuang" disabled="on" selected="on"></option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row"> 
                                                                <label for="jumlah" class="col-sm-2 col-form-label"> Jumlah*</label>
                                                                <div class="col-sm-10"> 
                                                                    <input type="number" class="form-control form-control-round" name="jumlah" id="jumlah" min="1" max="100" required="on" autocomplete="false" placeholder="Masukan jumlah barang" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group row"> 
                                                                <label for="jumlah" class="col-sm-2 col-form-label"> Sumber*</label>
                                                                <div class="col-sm-10"> 
                                                                    <input type="text" class="form-control form-control-round" name="sumber" id="sumber" min="1" max="100" required="on" autocomplete="false" placeholder="Masukan sumber barang" />
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <p><i>* Field Wajib diisi</i></p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" id="btn-simpan-kategori" class="btn waves-effect waves-dark btn-primary btn-outline-primary">Simpan</button>
                                                        <button type="button" class="btn waves-effect waves-dark btn-default btn-outline-danger" data-dismiss="modal" onclick="ClearFormData('#formBarang')">Batal</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="tab-pane" id="kategori" role="tabpanel">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Data Kategori 
                                        <button href="#modalKategori" data-toggle="modal" class="btn waves-effect waves-light btn-grd-primary" onclick="ClearFormData('#formKategori')">Tambah Data</button></h5>
                                        <button class="btn waves-effect waves-light btn-grd-primary" onclick="ReloadTable(tableKategori);"><i class="fas fa-refresh"></i></button></h5>
                                    </div>
                                    <div class="card-block">
                                        <table id="tableKategori" class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Kategori</th>
                                                    <th>Opsi</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Kategori</th>
                                                    <th>Opsi</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="modal fade  modal-flex" id="modalKategori" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Tambah Kategori</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                                <div class="modal-body model-container">
                                                    <form id="formKategori" method="post">
                                                        <input type="hidden" name="id" id="id">
                                                        <div class="form-group row"> 
                                                            <label class="col-sm-2 col-form-label"> Kategori* </label>
                                                            <div class="col-sm-10"> 
                                                                <input type="text" name="kategori" id="kategoridata" class="form-control form-control-round" autofocus="on" autocomplete="off" autosave="off" required="on" placeholder="Masukan data Kategori">
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <p><i>* Field Wajib diisi</i></p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" id="btn-simpan-kategori" class="btn waves-effect waves-dark btn-primary btn-outline-primary">Simpan</button>
                                                    <button type="button" class="btn waves-effect waves-dark btn-default btn-outline-danger" data-dismiss="modal" onclick="ClearFormData('#formKategori')">Batal</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var tableKategori, tableBarang, url, safe;
    $(document).ready(function() {
        url = "<?php safeURL(site_url('backend/getBarang')) ?>";
        safe = readURL(url);
        getKategori();
        tableBarang = $("#tableBarang").DataTable({
            // dom: 'Bfrtip',
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print', 'colvis'
            // ],
            ajax : {
                'type': 'POST',
                'url': safe
             },
            language: { "zeroRecords": "Tidak ada data" },
            responsive: 'true' 
        }); 
        url = "<?php safeURL(site_url('backend/getKategori')) ?>";
        safe = readURL(url);
        tableKategori = $("#tableKategori").DataTable({
            // dom: 'Bfrtip',
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print', 'colvis'
            // ],
            ajax : {
                'type': 'POST',
                'url': safe
             },
            language: { "zeroRecords": "Tidak ada data" },
            responsive: 'true' 
        });     
    });
    $("#formKategori").submit(function(event) {
        event.preventDefault();
        var data, url, safe, id = $("#id").val();
        url = "<?php safeURL(site_url('backend/simpanKategori')) ?>";
        if (id !== "") {
            url = "<?php safeURL(site_url('backend/updateKategori')) ?>";
        }
        safe = readURL(url);
        data = $(this).serialize();
        disabled("#btn-simpan-kategori");
        $.post(safe, data).done(function(data){
            if (data.status == true) {
                notif('Pemberitahuan', 'success', data.msg);
                ReloadTable(tableKategori);
                ClearFormData("#formKategori");
                undisabled("#btn-simpan-kategori");
            }else{
                ClearFormData("#formKategori");
                notif('Pemberitahuan', 'error', data.msg);
                undisabled("#btn-simpan-kategori");
            }
        }).fail(function(xhr, status, error) {
            ClearFormData("#formKategori");
            console.log('500 | Error save Kategori');
            undisabled("#btn-simpan-kategori");
        });
    });
    $("#tableKategori").on('click', '#edit-kategori', function(event) {
        event.preventDefault();
        $("#modalKategori").modal("show");
        var url, safe, id, form = $("#formKategori input");
            url = "<?php safeURL(site_url('backend/getKategoriById')) ?>";
            safe = readURL(url);
            id = $(this).data('id');
            form.prop('disabled', true);
            $.ajax({
                url:safe,
                data:{ id: id },
                dataType:'json',
                type:'post',
                success: function(data){
                    if (data) {
                        $("#id").val(data.id);
                        $("#kategoridata").val(data.kategori);
                        form.prop('disabled', false);
                    }else{
                        console.log("500 | getKategoriById false")
                        form.prop('disabled', false);
                    }
                },
                error: function(){
                    notif("Informasi", 'error', 'Ajax getKategoriById error');
                    console.log("Ajax getBarangById error");
                    form.prop('disabled', false);
                }
            });
    });
    $("#tableKategori").on('click', '#hapus-kategori', function(event) {
            event.preventDefault();
            var id = $(this).data('id'),
                url = "<?php safeURL(site_url('backend/hapusKategori')); ?>",
                safe = readURL(url);
                sweetAlert({
                  title: "Konfirmasi",
                  text: "Apakah anda yakin ingin menghapus data ini ?",
                  type: "warning",
                  showCancelButton: true,
                  cancelButtonText: "Batal",
                  cancelButtonColor: "#E8EAF6",
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Hapus",
                  closeOnConfirm: true
                },
                function(){
                    $.ajax({
                        url:safe,
                        data:{ id: id },
                        dataType:'json',
                        type:'post',
                        success: function(data){
                            if (data.status === true && data.code == 200) {
                                notif('Informasi', "success", data.msg);
                                setTimeout(function(){
                                    ReloadTable(tableKategori);
                                },200);
                            }else{
                                notif("Informasi",'info','Data gagal dihapus');
                            }
                        },
                        error: function(){
                            notif("Informasi", 'error', 'Ajax hapus Barang error');
                            console.log("Ajax hapus Kategori error");
                        }
                });
            });
    });


    $("#formBarang").submit(function(event) {
        event.preventDefault();
        var data, url, safe, id = $("#id").val();
        url = "<?php safeURL(site_url('backend/simpanBarang')) ?>";
        if (id !== "") {
            url = "<?php safeURL(site_url('backend/updateBarang')) ?>";
        }
        safe = readURL(url);
        data = $(this).serialize();
        disabled("#btn-simpan-kategori");
        $.post(safe, data).done(function(data){
            if (data.status == true) {
                notif('Pemberitahuan', 'success', data.msg);
                ReloadTable(tableBarang);
                ClearFormData("#formBarang");
                undisabled("#btn-simpan-kategori");
            }else{
                ClearFormData("#formBarang");
                notif('Pemberitahuan', 'error', data.msg);
                undisabled("#btn-simpan-kategori");
            }
        }).fail(function(xhr, status, error) {
            ClearFormData("#formBarang");
            console.log('500 | Error save Barang');
            undisabled("#btn-simpan-kategori");
        });
    });
    $("#tableBarang").on('click', '#edit-barang', function(event) {
        event.preventDefault();
        $("#modalBarang").modal("show");
        var url, safe, id, form = $("#formBarang input");
            url = "<?php safeURL(site_url('backend/getBarangById')) ?>";
            safe = readURL(url);
            id = $(this).data('id');
            form.prop('disabled', true);
            $.ajax({
                url:safe,
                data:{ id: id },
                dataType:'json',
                type:'post',
                success: function(data){
                    if (data) {
                        $("#id").val(data.id);
                        $("#Barang").val(data.nama);
                        form.prop('disabled', false);
                    }else{
                        console.log("500 | getBarangById false")
                        form.prop('disabled', false);
                    }
                },
                error: function(){
                    notif("Informasi", 'error', 'Ajax getBarangById error');
                    console.log("Ajax getBarangById error");
                    form.prop('disabled', false);
                }
            });
    });
    $("#tableBarang").on('click', '#hapus-barang', function(event) {
            event.preventDefault();
            var id = $(this).data('id'),
                url = "<?php safeURL(site_url('backend/hapusBarang')); ?>",
                safe = readURL(url);
                sweetAlert({
                  title: "Konfirmasi",
                  text: "Apakah anda yakin ingin menghapus data ini ?",
                  type: "warning",
                  showCancelButton: true,
                  cancelButtonText: "Batal",
                  cancelButtonColor: "#E8EAF6",
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Hapus",
                  closeOnConfirm: true
                },
                function(){
                    $.ajax({
                        url:safe,
                        data:{ id: id },
                        dataType:'json',
                        type:'post',
                        success: function(data){
                            if (data.status === true && data.code == 200) {
                                notif('Informasi', "success", data.msg);
                                setTimeout(function(){
                                    ReloadTable(tableBarang);
                                },200);
                            }else{
                                notif("Informasi",'info','Data gagal dihapus');
                            }
                        },
                        error: function(){
                            notif("Informasi", 'error', 'Ajax hapus Barang error');
                            console.log("Ajax hapus Barang error");
                        }
                });
            });
    });
    $("#data-kategori").on('change', function(event) {
        event.preventDefault();
        var id = $(this).val();
        getRuangByIdKategori(id);
    });
    function getKategori(){
            var url, safe;
                url = "<?php safeURL(site_url('backend/getKategoriData')) ?>";
                safe = readURL(url);
                $("#data-kategori").html('');
                $.post(safe).done(function(data){
                    $("#data-kategori").prop('disabled', false);
                    if (data) {
                        $("#defaultvalueDataKategori").text("Untuk kategori");
                        $.each(data, function(z, data) {
                            $("#data-kategori").append("<option value="+data.id+">"+data.kategori+"</option>");
                        });
                    }else{
                        $("#defaultvalueDataKategori").text("Pilih Data");
                    }
                }).fail(function(xhr, status, error) {
                    $("#defaultvalueDataKategori").text("").text("Gagal Mengambil data");
                });
    }
    function getRuangByIdKategori(id){
            var url, safe;
                url = "<?php safeURL(site_url('backend/getRuangByIdKategori')) ?>";
                safe = readURL(url);
                $("#data-ruang").html('');
                // $.post('/path/to/file', {param1: 'value1'}, function(data, textStatus, xhr) {
                    /*optional stuff to do after success */
                // });
                $.post(safe, {id: id}).done(function(data){
                    $("#data-ruang").prop('disabled', false);
                    if (data) {
                        $("#defaultvalueDataRuang").text("Untuk ruang");
                        $.each(data, function(z, data) {
                            $("#data-ruang").append("<option value="+data.id+">"+data.ruang+"</option>");
                        });
                    }else{
                        $("#defaultvalueDataRuang").text("Pilih Data");
                    }
                }).fail(function(xhr, status, error) {
                    $("#defaultvalueDataRuang").text("").text("Gagal Mengambil data");
                });
    }
</script>