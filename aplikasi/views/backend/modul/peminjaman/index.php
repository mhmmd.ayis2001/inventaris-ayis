<div class="main-body">
    <div class="page-wrapper">
        <div class="page-body">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Data Peminjaman
                            <button href="#modalPeminjaman" data-toggle="modal" class="btn waves-effect waves-light btn-grd-primary" onclick="ClearFormData('#formPeminjaman')">Pinjam Barang</button></h5>
                        </div>
                        <div class="card-block">
                            <table id="tablePeminjaman" class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Pinjam</th>
                                        <th>Jumlah</th>
                                        <th>Tanggal</th>
                                        <th>Durasi</th>
                                        <th>Status</th>
                                        <th>Kembali</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Pinjam</th>
                                        <th>Jumlah</th>
                                        <th>Tanggal</th>
                                        <th>Durasi</th>
                                        <th>Status</th>
                                        <th>Kembali</th>
                                        <th>Opsi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="modal fade  modal-flex" id="modalPeminjaman" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Pinjam Barang</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                    <div class="modal-body model-container">
                                        <form id="formPeminjaman" method="post">
                                            <input type="hidden" name="id" id="id">
                                            <?php
                                                if ($this->session->userdata('jabatan') === "Admin") { ?>
                                            <div class="form-group row"> 
                                                <label class="col-sm-2 col-form-label"> Pengguna*</label>
                                                <div class="col-sm-10"> 
                                                    <select id="data-pengguna" class="form-control" name="pengguna_" required="on" disabled="true">
                                                        <option id="defaultvalueDataPengguna" disabled="on" selected="on"></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <div class="form-group row"> 
                                                <label class="col-sm-2 col-form-label"> Barang*</label>
                                                <div class="col-sm-10"> 
                                                    <select id="data-barang" class="form-control" name="barang" required="on" disabled="true">
                                                        <option id="defaultvalueDataBarang" disabled="on" selected="on"></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row"> 
                                                <label class="col-sm-2 col-form-label"> Jumlah*</label>
                                                <div class="col-sm-10"> 
                                                    <input type="number" class="form-control form-control-round" name="jumlah" id="jumlah" min="1" max="3" required="on" autocomplete="false" placeholder="Jumlah barang" />
                                                </div>
                                            </div>
                                            <div class="form-group row"> 
                                                <label class="col-sm-2 col-form-label"> Durasi*</label>
                                                <div class="col-sm-10"> 
                                                    <input type="number" class="form-control form-control-round" name="durasi" id="durasi" min="1" max="7" required="on" autocomplete="false" placeholder="Durasi Peminjaman" />
                                                </div>
                                            </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" id="btn-simpan-peminjaman" class="btn waves-effect waves-dark btn-primary btn-outline-primary">Simpan</button>
                                        <button type="button" class="btn waves-effect waves-dark btn-default btn-outline-danger" data-dismiss="modal" onclick="ClearFormData('#formPeminjaman')">Batal</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade  modal-flex" id="modal-lapor-peminjaman" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Lapor Peminjaman</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                    <div class="modal-body model-container">
                                        <form id="formLaporPeminjaman" method="post">
                                            <input type="hidden" name="id" id="id">
                                             <div class="form-group row"> 
                                                <label class="col-sm-2 col-form-label"> Laporan*</label>
                                                <div class="col-sm-10"> 
                                                    <textarea id="laporan" name="laporan" class="form-control form-control-round" required="on" placeholder="Masukan laporan kamu"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row"> 
                                                <label class="col-sm-2 col-form-label"> Status*</label>
                                                <div class="col-sm-10"> 
                                                    <select id="status" name="status" class="form-control form-control-round" required="on">
                                                        <option value="rusak">Rusak</option>
                                                        <option value="hilang">Hilang</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <p>Setelah mengirim laporan, temui operator kepala bagian inventaris</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" id="btn-simpan-peminjaman" class="btn waves-effect waves-dark btn-primary btn-outline-primary">Simpan</button>
                                        <button type="button" class="btn waves-effect waves-dark btn-default btn-outline-danger" data-dismiss="modal" onclick="ClearFormData('#formPeminjaman')">Batal</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var tablePeminjaman, url, safe;
    $(document).ready(function() {
        url = "<?php safeURL(site_url('backend/getPeminjaman')) ?>";
        safe = readURL(url);
        getPengguna(); getBarang();
        tablePeminjaman = $("#tablePeminjaman").DataTable({
            'ajax' : {
                'type': 'POST',
                'url': safe
             },
            'language': { "zeroRecords": "<center>Tidak ada data</center>" },
            'responsive': 'true' 
        });     
    }); 

    $("#formPeminjaman").submit(function(event) {
        event.preventDefault();
        var data, url, safe, id = $("#id").val();
        url = "<?php safeURL(site_url('backend/simpanpeminjaman')) ?>";
        if (id !== "") {
            url = "<?php safeURL(site_url('backend/updatepeminjaman')) ?>";
        }
        safe = readURL(url);
        data = new FormData(this);
        disabled("#btn-simpan-peminjaman");
        $.ajax({
            url:safe,
            data: data,
            method: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data){
                if (data) {
                    notif('Pemberitahuan', 'success', data.msg);
                    ClearFormData("#formPeminjaman");
                    ReloadTable(tablePeminjaman);
                    undisabled("#btn-simpan-peminjaman");
                }else{
                    notif('Pemberitahuan', 'error', data.msg);
                    ClearFormData("#formPeminjaman");
                    ReloadTable(tablePeminjaman);
                    undisabled("#btn-simpan-peminjaman");
                }
            },
            error: function(){
                console.log('500 | Error save peminjaman');
                undisabled("#btn-simpan-peminjaman");
            }
        })
    });
    $("#tablePeminjaman").on('click', '#edit-peminjaman', function(event) {
        event.preventDefault();
        $("#modalPeminjaman").modal("show");
        var url, safe, id, attrRequiredIsRemove = $("#formPeminjaman input");
            url = "<?php safeURL(site_url('backend/getPeminjamanById')) ?>";
            safe = readURL(url);
            id = $(this).data('id');
            attrRequiredIsRemove.prop('required', false);
            attrRequiredIsRemove.prop('disabled', true)
        $.ajax({
                url:safe,
                data:{ id: id },
                dataType:'json',
                type:'post',
                success: function(data){
                    if (data) {
                        $("#id").val(data.id);

                    }else{
                        console.log("500 | getPeminjamanById false");
                        attrRequiredIsRemove.prop('disabled', false);
                    }
                },
                error: function(){
                    notif("Informasi", 'error', 'Ajax getPeminjamanById error');
                    console.log("Ajax getPeminjamanById error");
                    attrRequiredIsRemove.prop('disabled', false);
                }
            });
    });
    $("#tablePeminjaman").on('click', '#hapus-peminjaman', function(event) {
            event.preventDefault();
            var id = $(this).data('id'),
                url = "<?php safeURL(site_url('backend/hapuspeminjaman')); ?>",
                safe = readURL(url);
                sweetAlert({
                  title: "Konfirmasi",
                  text: "Apakah anda yakin ingin menghapus data ini ?",
                  type: "warning",
                  showCancelButton: true,
                  cancelButtonText: "Batal",
                  cancelButtonColor: "#E8EAF6",
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Hapus",
                  closeOnConfirm: true
                },
                function(){
                    $.ajax({
                        url:safe,
                        data:{ id: id },
                        dataType:'json',
                        type:'post',
                        success: function(data){
                            if (data.status === true && data.code == 200) {
                                notif('Informasi', "success", data.msg);
                                setTimeout(function(){
                                    ReloadTable(tablePeminjaman);
                                },200);
                            }else{
                                notif("Informasi",'info','Data gagal dihapus');
                            }
                        },
                        error: function(){
                            notif("Informasi", 'error', 'Ajax hapus peminjaman error');
                            console.log("Ajax hapus peminjaman error");
                        }
                });
            });
    });
    $("#tablePeminjaman").on('click', '#konfirmasi-peminjaman', function(event) {
            event.preventDefault();
            var id = $(this).data('id'),
                url = "<?php safeURL(site_url('backend/konfirmasiPeminjaman')); ?>",
                safe = readURL(url);
                sweetAlert({
                  title: "Konfirmasi",
                  text: "Apakah barang akan dikembalikan sekarang ?",
                  type: "info",
                  showCancelButton: true,
                  cancelButtonText: "Batal",
                  cancelButtonColor: "#E8EAF6",
                  confirmButtonColor: "#0378ff !important",
                  confirmButtonText: "Konfirmasi",
                  closeOnConfirm: true
                },
                function(){
                    $.ajax({
                        url:safe,
                        data:{ id: id },
                        dataType:'json',
                        type:'post',
                        success: function(data){
                            if (data.status === true && data.code == 200) {
                                notif('Informasi', "success", data.msg);
                                setTimeout(function(){
                                    ReloadTable(tablePeminjaman);
                                },200);
                            }else{
                                notif("Informasi",'info','Data gagal dikonfirmasi');
                            }
                        },
                        error: function(){
                            notif("Informasi", 'error', 'Ajax konfirmasi peminjaman error');
                            console.log("Ajax konfirmasi peminjaman error");
                        }
                });
            });
    });
    $("#tablePeminjaman").on('click', '#lapor-peminjaman', function(event) {
        event.preventDefault();
        $("#modal-lapor-peminjaman").modal();
    });
    function getPengguna(){
        var url, safe;
            url = "<?php safeURL(site_url('backend/getPenggunaData')) ?>";
            safe = readURL(url);
            $("#data-pengguna").html('');
            $.post(safe).done(function(data){
                $("#data-pengguna").prop('disabled', false);
                if (data) {
                    $("#defaultvalueDataPengguna").text("Pilih Pengguna");
                    $.each(data, function(z, data) {
                        $("#data-pengguna").append("<option value="+data.id+">"+data.nama+"</option>");
                    });
                }else{
                    $("#defaultvalueDataPengguna").text("Pilih Data");
                }
            }).fail(function(xhr, status, error) {
                $("#defaultvalueDataPengguna").text("").text("Gagal Mengambil data");
            });
    }
    function getBarang(){
        var url, safe;
            url = "<?php safeURL(site_url('backend/getBarangData')) ?>";
            safe = readURL(url);
            $("#data-barang").html('');
            $.post(safe).done(function(data){
                $("#data-barang").prop('disabled', false);
                if (data) {
                    $("#defaultvalueDataBarang").text("Pilih barang");
                    $.each(data, function(z, data) {
                        $("#data-barang").append("<option value="+data.id+">"+data.barang+"</option>");
                    });
                }else{
                    $("#defaultvalueDataBarang").text("Pilih Data");
                }
            }).fail(function(xhr, status, error) {
                $("#defaultvalueDataBarang").text("").text("Gagal Mengambil data");
            });
    }
</script>