<div class="main-body">
    <div class="page-wrapper">
        <div class="page-body">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Data Ruang 
                            <button href="#modalRuang" data-toggle="modal" class="btn waves-effect waves-light btn-grd-primary" onclick="ClearFormData('#formRuang')">Tambah Data</button></h5>
                        </div>
                        <div class="card-block">
                            <table id="tableRuang" class="table">
                                <thead>
                                    <tr>
                                        <th><center>#</center></th>
                                        <th><center>Ruang</center></th>
                                        <th><center>Kategori</center></th>
                                        <th><center>Opsi</center></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th><center>#</center></th>
                                        <th><center>Ruang</center></th>
                                        <th><center>Kategori</center></th>
                                        <th><center>Opsi</center></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="modal fade  modal-flex" id="modalRuang" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Tambah Ruang</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                    <div class="modal-body model-container">
                                        <form id="formRuang" method="post">
                                            <input type="hidden" name="id" id="id">
                                            <div class="form-group row"> 
                                                <label class="col-sm-2 col-form-label"> Ruang* </label>
                                                <div class="col-sm-10"> 
                                                    <input type="text" name="ruang" id="ruang" class="form-control form-control-round" autofocus="on" autocomplete="off" autosave="off" required="on" placeholder="Masukan data Ruang">
                                                </div>
                                            </div>
                                            <div class="form-group row"> 
                                                <label class="col-sm-2 col-form-label"> Kategori*</label>
                                                <div class="col-sm-10"> 
                                                    <select id="data-kategori" class="form-control" name="kategori" required="on" disabled="true">
                                                        <option id="defaultvalueDataKategori" disabled="on" selected="on"></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                            <p><i>* Field Wajib diisi</i></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" id="btn-simpan-Ruang" class="btn waves-effect waves-dark btn-primary btn-outline-primary">Simpan</button>
                                        <button type="button" class="btn waves-effect waves-dark btn-default btn-outline-danger" data-dismiss="modal" onclick="ClearFormData('#formRuang')">Batal</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var tableRuang, url, safe;
    $(document).ready(function() {
        url = "<?php safeURL(site_url('backend/getRuang')) ?>";
        safe = readURL(url);
        getKategori();
        tableRuang = $("#tableRuang").DataTable({
            // dom: 'Bfrtip',
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print', 'colvis'
            // ],
            ajax : {
                'type': 'POST',
                'url': safe
             },
            language: { "zeroRecords": "<center>Tidak ada data</center>" },
            responsive: 'true' 
        });     
    }); 
    $("#formRuang").submit(function(event) {
        event.preventDefault();
        var data, url, safe, id = $("#id").val();
        url = "<?php safeURL(site_url('backend/simpanRuang')) ?>";
        if (id !== "") {
            url = "<?php safeURL(site_url('backend/updateRuang')) ?>";
        }
        safe = readURL(url);
        data = $(this).serialize();
        disabled("#btn-simpan-Ruang");
        $.post(safe, data).done(function(data){
            if (data.status == true) {
                notif('Pemberitahuan', 'success', data.msg);
                ReloadTable(tableRuang);
                ClearFormData("#formRuang");
                undisabled("#btn-simpan-Ruang");
            }else{
                ClearFormData("#formRuang");
                notif('Pemberitahuan', 'error', data.msg);
                undisabled("#btn-simpan-Ruang");
            }
        }).fail(function(xhr, status, error) {
            ClearFormData("#formRuang");
            console.log('500 | Error save Ruang');
            undisabled("#btn-simpan-Ruang");
        });
    });
    $("#tableRuang").on('click', '#edit-ruang', function(event) {
        event.preventDefault();
        $("#modalRuang").modal("show");
        var url, safe, id, form = $("#formRuang input");
            url = "<?php safeURL(site_url('backend/getRuangById')) ?>";
            safe = readURL(url);
            id = $(this).data('id');
            form.prop('disabled', true);
            $.ajax({
                url:safe,
                data:{ id: id },
                dataType:'json',
                type:'post',
                success: function(data){
                    if (data) {
                        $("#id").val(data.id);
                        $("#ruang").val(data.Ruang);
                        $("#kategori").val(data.Ruang);
                        form.prop('disabled', false);
                    }else{
                        console.log("500 | getRuangById false")
                        form.prop('disabled', false);
                    }
                },
                error: function(){
                    notif("Informasi", 'error', 'Ajax getRuangById error');
                    console.log("Ajax getRuangById error");
                    form.prop('disabled', false);
                }
            });
    });
    $("#tableRuang").on('click', '#hapus-ruang', function(event) {
            event.preventDefault();
            var id = $(this).data('id'),
                url = "<?php safeURL(site_url('backend/hapusRuang')); ?>",
                safe = readURL(url);
                sweetAlert({
                  title: "Konfirmasi",
                  text: "Apakah anda yakin ingin menghapus data ini ?",
                  type: "warning",
                  showCancelButton: true,
                  cancelButtonText: "Batal",
                  cancelButtonColor: "#E8EAF6",
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Hapus",
                  closeOnConfirm: true
                },
                function(){
                    $.ajax({
                        url:safe,
                        data:{ id: id },
                        dataType:'json',
                        type:'post',
                        success: function(data){
                            if (data.status === true && data.code == 200) {
                                notif('Informasi', "success", data.msg);
                                setTimeout(function(){
                                    ReloadTable(tableRuang);
                                },200);
                            }else{
                                notif("Informasi",'info','Data gagal dihapus');
                            }
                        },
                        error: function(){
                            notif("Informasi", 'error', 'Ajax hapus Ruang error');
                            console.log("Ajax hapus Ruang error");
                        }
                });
            });
    });
    function getKategori(){
            var url, safe;
                url = "<?php safeURL(site_url('backend/getKategoriData')) ?>";
                safe = readURL(url);
                $("#data-kategori").html('');
                $.post(safe).done(function(data){
                    $("#data-kategori").prop('disabled', false);
                    if (data) {
                        $("#defaultvalueDataKategori").text("Untuk kategori");
                        $.each(data, function(z, data) {
                            $("#data-kategori").append("<option value="+data.id+">"+data.kategori+"</option>");
                        });
                    }else{
                        $("#defaultvalueDatakategori").text("Pilih Data");
                    }
                }).fail(function(xhr, status, error) {
                    $("#defaultvalueDatakategori").text("").text("Gagal Mengambil data");
                });
        }
</script>