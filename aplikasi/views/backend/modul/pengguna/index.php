<div class="main-body">
    <div class="page-wrapper">
        <div class="page-body">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Data Pengguna 
                            <button href="#modalPengguna" data-toggle="modal" class="btn waves-effect waves-light btn-grd-primary" onclick="ClearFormData('#formPengguna')">Tambah Data</button></h5>
                        </div>
                        <div class="card-block">
                            <table id="tablePengguna" class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>Email</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>Email</th>
                                        <th>Opsi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="modal fade  modal-flex" id="modalPengguna" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Tambah Pengguna</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                    <div class="modal-body model-container">
                                        <form id="formPengguna" method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="id" id="id">
                                            <div class="form-group row"> 
                                                <label class="col-sm-2 col-form-label"> Nama*</label>
                                                <div class="col-sm-10"> 
                                                    <input type="text" name="nama" id="nama" class="form-control form-control-round" autofocus="on" autocomplete="off" autosave="off" required="on">
                                                </div>
                                            </div>
                                            <div class="form-group row"> 
                                                <label class="col-sm-2 col-form-label"> Jabatan*</label>
                                                <div class="col-sm-10"> 
                                                    <select id="data-jabatan" class="form-control" name="jabatan" required="on" disabled="true">
                                                        <option id="defaultvalueDataJabatan" disabled="on" selected="on"></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row"> 
                                                <label class="col-sm-2 col-form-label"> Foto*</label>
                                                <div class="col-sm-10"> 
                                                    <input type="file" name="file" id="file" required="on"/>
                                                </div>
                                            </div>
                                            <div class="form-group row"> 
                                                <label class="col-sm-2 col-form-label"> Email*</label>
                                                <div class="col-sm-10"> 
                                                    <input type="email" name="email" id="email" class="form-control form-control-round" required="on" autofocus="on" autocomplete="off" autosave="off">
                                                </div>
                                            </div>
                                            <div class="form-group row"> 
                                                <label class="col-sm-2 col-form-label"> Username*</label>
                                                <div class="col-sm-10"> 
                                                    <input type="text" name="username" id="username" class="form-control form-control-round" required="on" autofocus="on" autocomplete="off" autosave="off">
                                                </div>
                                            </div>
                                            <div class="form-group row"> 
                                                <label class="col-sm-2 col-form-label"> Password*</label>
                                                <div class="col-sm-10"> 
                                                    <input type="password" name="password" id="password" class="form-control form-control-round" required="on" autofocus="on" autocomplete="off" autosave="off">
                                                </div>
                                            </div>
                                            <br>
                                            <p><i>* Field Wajib diisi</i></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" id="btn-simpan-Pengguna" class="btn waves-effect waves-dark btn-primary btn-outline-primary">Simpan</button>
                                        <button type="button" class="btn waves-effect waves-dark btn-default btn-outline-danger" data-dismiss="modal" onclick="ClearFormData('#formPengguna')">Batal</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var tablePengguna, url, safe;
    $(document).ready(function() {
        url = "<?php safeURL(site_url('backend/getPengguna')) ?>";
        safe = readURL(url);
        getJabatan();
        tablePengguna = $("#tablePengguna").DataTable({
            'ajax' : {
                'type': 'POST',
                'url': safe
             },
            'language': { "zeroRecords": "<center>Tidak ada data</center>" },
            'responsive': 'true' 
        });     
    }); 

    $("#formPengguna").submit(function(event) {
        event.preventDefault();
        var data, url, safe, id = $("#id").val();
        url = "<?php safeURL(site_url('backend/simpanPengguna')) ?>";
        if (id !== "") {
            url = "<?php safeURL(site_url('backend/updatePengguna')) ?>";
        }
        safe = readURL(url);
        data = new FormData(this);
        disabled("#btn-simpan-Pengguna");
        $.ajax({
            url:safe,
            data: data,
            method: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data){
                if (data) {
                    notif('Pemberitahuan', 'success', data.msg);
                    ClearFormData("#formPengguna");
                    ReloadTable(tablePengguna);
                    undisabled("#btn-simpan-Pengguna");
                }else{
                    notif('Pemberitahuan', 'error', data.msg);
                    ClearFormData("#formPengguna");
                    ReloadTable(tablePengguna);
                    undisabled("#btn-simpan-Pengguna");
                }
            },
            error: function(){
                console.log('500 | Error save Pengguna');
                undisabled("#btn-simpan-Pengguna");
            }
        })
    });
    $("#tablePengguna").on('click', '#edit-pengguna', function(event) {
        event.preventDefault();
        $("#modalPengguna").modal("show");
        var url, safe, id, attrRequiredIsRemove = $("#formPengguna input");
            url = "<?php safeURL(site_url('backend/getPenggunaById')) ?>";
            safe = readURL(url);
            id = $(this).data('id');
            attrRequiredIsRemove.prop('required', false);
            attrRequiredIsRemove.prop('disabled', true)
        $.ajax({
                url:safe,
                data:{ id: id },
                dataType:'json',
                type:'post',
                success: function(data){
                    if (data) {
                        $("#id").val(data.id);
                        $("#nama").val(data.nama);
                        $("#jabatan").val(data.jabatan);
                        $("#email").val(data.email);
                        $("#username").val(data.username);
                        var password = data.katasandi;
                        // set to localstroge;
                        localStorage.setItem('password', password);
                        localStorage.setItem('file', data.file);
                        attrRequiredIsRemove.prop('disabled', false);
                        // getting password and convert
                    }else{
                        console.log("500 | getPenggunaById false");
                        attrRequiredIsRemove.prop('disabled', false);
                    }
                },
                error: function(){
                    notif("Informasi", 'error', 'Ajax getPenggunaById error');
                    console.log("Ajax getPenggunaById error");
                    attrRequiredIsRemove.prop('disabled', false);
                }
            });
    });
    $("#tablePengguna").on('click', '#hapus-pengguna', function(event) {
            event.preventDefault();
            var id = $(this).data('id'),
                url = "<?php safeURL(site_url('backend/hapusPengguna')); ?>",
                safe = readURL(url);
                sweetAlert({
                  title: "Konfirmasi",
                  text: "Apakah anda yakin ingin menghapus data ini ?",
                  type: "warning",
                  showCancelButton: true,
                  cancelButtonText: "Batal",
                  cancelButtonColor: "#E8EAF6",
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Hapus",
                  closeOnConfirm: true
                },
                function(){
                    $.ajax({
                        url:safe,
                        data:{ id: id },
                        dataType:'json',
                        type:'post',
                        success: function(data){
                            if (data.status === true && data.code == 200) {
                                notif('Informasi', "success", data.msg);
                                setTimeout(function(){
                                    ReloadTable(tablePengguna);
                                },200);
                            }else{
                                notif("Informasi",'info','Data gagal dihapus');
                            }
                        },
                        error: function(){
                            notif("Informasi", 'error', 'Ajax hapus Pengguna error');
                            console.log("Ajax hapus Pengguna error");
                        }
                });
            });
        });
        function getJabatan(){
            var url, safe;
                url = "<?php safeURL(site_url('backend/getJabatanData')) ?>";
                safe = readURL(url);
                $("#data-jabatan").html('');
                $.post(safe).done(function(data){
                    $("#data-jabatan").prop('disabled', false);
                    if (data) {
                        $("#defaultvalueDataJabatan").text("Untuk Jabatan");
                        $.each(data, function(z, data) {
                            $("#data-jabatan").append("<option value="+data.id+">"+data.jabatan+"</option>");
                        });
                    }else{
                        $("#defaultvalueDataJabatan").text("Pilih Data");
                    }
                }).fail(function(xhr, status, error) {
                    $("#defaultvalueDataJabatan").text("").text("Gagal Mengambil data");
                });
        }
</script>