<div class="main-body">
    <div class="page-wrapper">
        <div class="page-body">
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <a href="<?php echo site_url('') ?>">
                        <div class="card prod-p-card card-red">
                            <div class="card-body">
                                <div class="row align-items-center m-b-30">
                                    <div class="col">
                                        <h6 class="m-b-5 text-white"></h6>
                                        <h3 class="m-b-0 f-w-700 text-white" id="jumlahSuratMasukDashboard"></h3>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-envelope text-c-red f-18"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-md-6">
                    <a href="<?php echo site_url('') ?>">
                        <div class="card prod-p-card card-blue">
                            <div class="card-body">
                                <div class="row align-items-center m-b-30">
                                    <div class="col">
                                        <h6 class="m-b-5 text-white"></h6>
                                        <h3 class="m-b-0 f-w-700 text-white" id="jumlahSuratKeluarDashboard"></h3>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-envelope-open text-c-blue f-18"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-md-6">
                    <a href="<?php echo site_url('') ?>">
                        <div class="card prod-p-card card-green">
                            <div class="card-body">
                                <div class="row align-items-center m-b-30">
                                    <div class="col">
                                        <h6 class="m-b-5 text-white"></h6>
                                        <h3 class="m-b-0 f-w-700 text-white" id="jumlahNomorSuratDashboard"></h3>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-fax text-c-green f-18"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-md-6">
                    <a href="<?php echo site_url('') ?>">
                        <div class="card prod-p-card card-yellow">
                            <div class="card-body">
                                <div class="row align-items-center m-b-30">
                                    <div class="col">
                                        <h6 class="m-b-5 text-white"></h6>
                                        <h3 class="m-b-0 f-w-700 text-white" id="jumlahDashboard"></h3>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-file text-c-yellow f-18"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xl-6 col-md-12">
                    <div class="card latest-update-card">
                        <div class="card-header">
                            <h5> Terbaru</h5>
                            <div class="card-header-right">
                                <ul class="list-unstyled card-option">
                                    <li class="first-opt"><i class="feather icon-chevron-left open-card-option"></i></li>
                                    <li><i class="feather icon-maximize full-card"></i></li>
                                    <li><i class="feather icon-minus minimize-card"></i></li>
                                    <li><i class="feather icon-refresh-cw reload-card"></i></li>
                                    <li><i class="feather icon-trash close-card"></i></li>
                                    <li><i class="feather icon-chevron-left open-card-option"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="latest-update-box" id="latest-update-box">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-12">
                    <div class="card new-cust-card">
                        <div class="card-header">
                            <h5> Terbaru</h5>
                            <div class="card-header-right">
                                <ul class="list-unstyled card-option">
                                    <li class="first-opt"><i class="feather icon-chevron-left open-card-option"></i></li>
                                    <li><i class="feather icon-maximize full-card"></i></li>
                                    <li><i class="feather icon-minus minimize-card"></i></li>
                                    <li><i class="feather icon-refresh-cw reload-card"></i></li>
                                    <li><i class="feather icon-trash close-card"></i></li>
                                    <li><i class="feather icon-chevron-left open-card-option"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-block" id="latest-update-box-surat-keluar">
                        </div>
                    </div>
                </div>

                <div class="col-xl-12 col-md-12">
                    <div class="card latest-update-card">
                        <div class="card-header">
                            <h5>Aktivitas Terakhir</h5>
                            <div class="card-header-right">
                                <ul class="list-unstyled card-option">
                                    <li class="first-opt"><i class="feather icon-chevron-left open-card-option"></i></li>
                                    <li><i class="feather icon-maximize full-card"></i></li>
                                    <li><i class="feather icon-minus minimize-card"></i></li>
                                    <li><i class="feather icon-refresh-cw reload-card"></i></li>
                                    <li><i class="feather icon-trash close-card"></i></li>
                                    <li><i class="feather icon-chevron-left open-card-option"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="latest-update-box" id="latest-update-box-activity">
                            </div>
                            <div class="text-right">
                                <a href="<?php echo site_url('logaktivitas') ?>" class=" b-b-primary text-primary"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    url = "<?php safeURL(site_url('backend/getLatestUpdateAktivitas')) ?>";
    safe = readURL(url);
    $.post(safe, function(data, textStatus, xhr) {
        if (xhr.status == 200) {
            var i; 
            for (i=0; i < data.length;i++) {
                var kategori = data[i].kategori, icon;
                    if (kategori === "login") {
                        icon = 'fas fa-sign-in-alt';
                    }else if (kategori === "logout") {
                        icon = 'fas fa-sign-out-alt';
                    }else if (kategori === "menyimpan") {
                        icon = 'fas fa-save';
                    }else if (kategori === "memperbarui") {
                        icon = 'fas fa-edit';
                    }else if (kategori === "menghapus") {
                        icon = 'fas fa-trash';
                    }else if (kategori === "kirim email") {
                        icon = 'fas fa-envelope';
                    }
                    else{
                        icon = 'feather icon-file-text';
                    }
                var html = '<div class="row p-b-30"> <div class="col-auto text-right update-meta"> <i class="'+icon+' bg-c-blue update-icon"></i> </div> <div class="col"> <h6>'+data[i].siapa+'</h6> <p class="text-muted m-b-0">'+data[i].aktivitas+'</p> </div> </div>'; 
                $("#latest-update-box-activity").append(html);
            }
        }
    });
</script>